<?php
try{
    $req = new stdClass();
    $req->PartNumber = '9010048-02';
    $client = new SoapClient('http://'.$_SERVER['HTTP_HOST'].'?wsdl', ['trace' => 1, 'soap_version' => SOAP_1_2]);
    $result = $client->GetAvailableInventoryPerSku($req);
    var_dump(json_decode(json_encode($result)));
    //echo $client->__getLastResponse();
}catch(\Exception $e){
    die("Error:".$e->getMessage().' Line:'.$e->getLine());
}
