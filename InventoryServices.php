<?php

/**
 * Created by PhpStorm.
 * User: Ilya
 * Date: 3/21/2017
 * Time: 4:20 PM
 */
class InventoryServices
{
    private $db_connection;

    public function __construct()
    {
        /*DB connection*/
        $this->db_connection = new PDO('mysql:host=localhost;dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASS);
    }

    public function GetAvailableInventoryPerSku($request)
    {

        try{
            $resp = new resp();
            $resp->messageType = 'GlobalInventory';
            $resp->requestServiceName = 'GetGlobalInventory';
            $resp->dateTimeStamp = date('Y-m-d\TH:i:s');


            $where = '';
            if(isset($request->WareHouseCode) && !empty($request->WareHouseCode)) {
                /*Receive list of stock*/
                $where .=  (empty($where)) ? " where `ssl`='$request->WareHouseCode'" : " and `ssl`='$request->WareHouseCode' ";
            }

            if(isset($request->GoodBad) && !empty($request->GoodBad)){
                /*Good or Bad*/
                $where .=  (empty($where)) ? " where `gb`='$request->GoodBad'" : " and `gb`='$request->GoodBad' ";
            }

            if(isset($request->PartNumber) && !empty($request->PartNumber)){
                /*Good or Bad*/
                $where .=  (empty($where)) ? " where `part`='$request->PartNumber'" : " and `part`='$request->GoodBad'";
            }

            $items = $this->db_connection->query('SELECT * FROM `spectrum_dynamics_stocks`'.$where);



            if(count($items) > 0){
                $counter = 0;
                foreach ($items as $item){
                    $data[$counter] = new Data;
                    $data[$counter]->warehouse = $item['ssl'];
                    $data[$counter]->sku = $item['part'];
                    $data[$counter]->status = "AVAILABLE";
                    $data[$counter]->expirydate = '';
                    $data[$counter]->serial = $item['serial'];
                    $data[$counter]->batch = $item['gb'];
                    $data[$counter]->qty = floatval($item['onhand']);
                    $resp->dataList[] = $data[$counter];
                }
            }
            return $resp;
        }catch(\Exception $e){
            die("Error:".$e->getMessage().' Line:'.$e->getLine());
        }
    }

    public function GetGlobalInventory($request)
    {

        try{
            $resp = new resp();
            $resp->messageType = 'GlobalInventory';
            $resp->requestServiceName = 'GetGlobalInventory';
            $resp->dateTimeStamp = date('Y-m-d H:i:s');

            /*Setup items*/
            $where = '';
            if(isset($request->WareHouseCode) && !empty($request->WareHouseCode)) {
                /*Receive list of stock*/
                $where .=  (empty($where)) ? " where `ssl`='$request->WareHouseCode'" : " and `ssl`='$request->WareHouseCode' ";
            }

            if(isset($request->GoodBad) && !empty($request->GoodBad)){
                /*Good or Bad*/
                $where .=  (empty($where)) ? " where `gb`='$request->GoodBad'" : " and `gb`='$request->GoodBad' ";
            }

            $items = $this->db_connection->query('SELECT * FROM `spectrum_dynamics_stocks`'.$where);


            if(count($items) > 0){
                $counter = 0;
                foreach ($items as $item){
                    $data[$counter] = new Data;
                    $data[$counter]->warehouse = $item['ssl'];
                    $data[$counter]->sku = $item['part'];
                    $data[$counter]->status = "AVAILABLE";
                    $data[$counter]->expirydate = '';
                    $data[$counter]->serial = $item['serial'];
                    $data[$counter]->batch = $item['gb'];
                    $data[$counter]->qty = floatval($item['onhand']);
                    $resp->dataList[] = $data[$counter];
                }
            }

            return $resp;
        }catch(\Exception $e){
            die("Error:".$e->getMessage().' Line:'.$e->getLine());
        }
    }

}

/*Item within WH*/
class Data{
    public $warehouse;
    public $sku;
    public $status;
    public $expirydate;
    public $serial;
    public $batch;
    public $qty;
}

/*Items within WH*/
class dataList{
    public $data;
}

/*Response*/
class resp{
    public $messageType;
    public $requestServiceName;
    public $dateTimeStamp = '';
    public $dataList;
}