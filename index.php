<?php 
/*Entering point will be server side*/

//Disable cache
ini_set("soap.wsdl_cache_enabled", "0");
//Setup full url
define('URL',"http://$_SERVER[HTTP_HOST]");
/*DB*/
define('DB_USER',"root");
define('DB_PASS',"");
define('DB_NAME',"soap");


/*wsdl file*/
if(isset($_GET['wsdl'])){
    header("Content-type: text/xml");
    ob_flush();
    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    /*
     * Make sure manually change path in wsdl file for <soap:address location=
     *
     * */
    require_once('main.wsdl');
    ob_clean();
    exit();
}


/*server side*/
if(isset($_GET['Inventory'])) {
    require_once("InventoryServices.php");
    $server = new SoapServer(URL.'/?wsdl', ['cache_wsdl' => WSDL_CACHE_NONE]);
    $server->setClass(InventoryServices::class);
    $server->handle();

}
die();

?>